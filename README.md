This feature allows to use the openmpi wrapper compilers with the qmake build system by simply adding the following to the .pro file:

```
#!qmake
CONFIG += openmpi
```


To enable add the .prf file in $QTDIR/mkspecs/features